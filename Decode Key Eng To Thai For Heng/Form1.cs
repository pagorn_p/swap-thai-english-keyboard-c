﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Decode_Key_Eng_To_Thai_For_Heng
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(textBox2.Text);
        }

        private string decodeTyping(string hengType, bool isEnToTh, bool isCapslock)
        {
            if (isCapslock && isEnToTh) {
                hengType = hengType.ToLower();
            } else if (isCapslock && !isEnToTh) { //เปิดแคปล็อกไว้ไม่พอ ยังจะสลับไทยเป็นอิ้ง
                hengType = switchCapslock(hengType);
            }


            string sentence = "";
            Char[] tmp = hengType.ToCharArray();

            for (int i = 0; i < hengType.Length; i++)
            {
                if (isEnToTh)
                {
                    sentence += switchKeyEngToThaiChar(tmp[i]);
                }
                else {
                    sentence += switchKeyThaiToEngChar(tmp[i]);
                }
            }

            return sentence;
        }

        private string switchCapslock(string hengType) {
            string sentence = "";
            Char[] tmp = hengType.ToCharArray();

            for (int i = 0; i < hengType.Length; i++)
            {
                sentence += switchCapslockChar(tmp[i]);
            }

            return sentence;
        }

        private Char switchCapslockChar(Char c)
        {
            switch (c)
            {
                case '+': return 'ๅ';
                case '๑': return '/';
                case '๒': return '-';
                case '๓': return 'ภ';
                case '๔': return 'ถ';
                case 'ู': return 'ุ';
                case '฿': return 'ึ';
                case '๕': return 'ค';
                case '๖': return 'ต';
                case '๗': return 'จ';
                case '๘': return 'ข';
                case '๙': return 'ช';
                case '๐': return 'ๆ';
                case '\"': return 'ไ';
                case 'ฎ': return 'ำ';
                case 'ฑ': return 'พ';
                case 'ธ': return 'ะ';
                case 'ํ': return 'ั';
                case '๊': return 'ี';
                case 'ณ': return 'ร';
                case 'ฯ': return 'น';
                case 'ญ': return 'ย';
                case 'ฐ': return 'บ';
                case ',': return 'ล';
                case 'ฅ': return 'ฃ';

                case 'ฤ': return 'ฟ';
                case 'ฆ': return 'ห';
                case 'ฏ': return 'ก';
                case 'โ': return 'ด';
                case 'ฌ': return 'เ';
                case '็': return '้';
                case '๋': return '่';
                case 'ษ': return 'า';
                case 'ศ': return 'ส';
                case 'ซ': return 'ว';
                case '.': return 'ง'; 
                case '(': return 'ผ';
                case ')': return 'ป';
                case 'ฉ': return 'แ';
                case 'ฮ': return 'อ';
                case 'ฺ': return 'ิ';
                case '์': return 'ื';
                case '?': return 'ท';
                case 'ฒ': return 'ม';
                case 'ฬ': return 'ใ';
                case 'ฦ': return 'ฝ'; 

                default:
                    return c;
            }
        }

        private char switchKeyThaiToEngChar(char c)
        {
            switch (c)
            {
                case 'ๅ': return '1'; 
                case '+': return '!'; 
                case '/': return '2'; 
                case '๑': return '@';
                case '-': return '3';
                case '๒': return '#';
                case '4': return 'ภ';
                case '$': return '๓';
                case '5': return 'ถ';
                case '%': return '๔';
                case 'ุ': return '6';
                case 'ู': return '^';
                case 'ึ': return '7';
                case '฿': return '&';
                case 'ค': return '8'; 
                case '๕': return '*';
                case 'ต': return '9';
                case '๖': return '('; 
                case 'จ': return '0';
                case '๗': return ')';
                case 'ข': return '-';
                case '๘': return '_';
                case 'ช': return '=';
                case '๙': return '+';
                case 'ฃ': return '\\';
                case 'ฅ': return '|';

                case 'ๆ': return 'q';
                case '๐': return 'Q';
                case 'ไ': return 'w';
                case '\"': return 'W';
                case 'ำ': return 'e';
                case 'ฎ': return 'E';
                case 'พ': return 'r';
                case 'ฑ': return 'R';
                case 'ะ': return 't';
                case 'ธ': return 'T';
                case 'ั': return 'y';
                case 'ํ': return 'Y';
                case 'ี': return 'u';
                case '๊': return 'U';
                case 'ร': return 'i'; 
                case 'ณ': return 'I';
                case 'น': return 'o';
                case 'ฯ': return 'O';
                case 'ย': return 'p';
                case 'ญ': return 'P';
                case 'บ': return '[';
                case 'ฐ': return '{';
                case 'ล': return ']';
                case ',': return '}';


                case 'ฟ': return 'a';
                case 'ฤ': return 'A';
                case 'ห': return 's';
                case 'ฆ': return 'S';
                case 'ก': return 'd';
                case 'ฏ': return 'D';
                case 'ด': return 'f';
                case 'โ': return 'F';
                case 'เ': return 'g';
                case 'ฌ': return 'G';
                case '้': return 'h';
                case '็': return 'H';
                case '่': return 'j';
                case '๋': return 'J';
                case 'า': return 'k';
                case 'ษ': return 'K';
                case 'ส': return 'l';
                case 'ศ': return 'L';
                case 'ว': return ';';
                case 'ซ': return ':';
                case 'ง': return '\'';
                case '.': return '\"';

                case 'ผ': return 'z';
                case '(': return 'Z';
                case 'ป': return 'x';
                case ')': return 'X';
                case 'แ': return 'c';
                case 'ฉ': return 'C';
                case 'อ': return 'v';
                case 'ฮ': return 'V';
                case 'ิ': return 'b';
                case 'ฺ': return 'B';
                case 'ื': return 'n';
                case '์': return 'N';
                case 'ท': return 'm';
                case '?': return 'M';
                case 'ม': return ',';
                case 'ฒ': return '<';
                case 'ใ': return '.';
                case 'ฬ': return '>';
                case 'ฝ': return '/';
                case 'ฦ': return '?';
                default:
                    return c;
            }
        }

        private char switchKeyEngToThaiChar(char c)
        {
            switch (c)
            {
                case '1': return 'ๅ';
                case '!': return '+';
                case '2': return '/';
                case '@': return '๑';
                case '3': return '-';
                case '#': return '๒';
                case '4': return 'ภ';
                case '$': return '๓';
                case '5': return 'ถ';
                case '%': return '๔';
                case '6': return 'ุ';
                case '^': return 'ู';
                case '7': return 'ึ';
                case '&': return '฿';
                case '8': return 'ค';
                case '*': return '๕';
                case '9': return 'ต';
                case '(': return '๖';
                case '0': return 'จ';
                case ')': return '๗';
                case '-': return 'ข';
                case '_': return '๘';
                case '=': return 'ช';
                case '+': return '๙';
                case '\\': return 'ฃ';
                case '|': return 'ฅ';

                case 'q': return 'ๆ';
                case 'Q': return '๐';
                case 'w': return 'ไ';
                case 'W': return '\"';
                case 'e': return 'ำ';
                case 'E': return 'ฎ';
                case 'r': return 'พ';
                case 'R': return 'ฑ';
                case 't': return 'ะ';
                case 'T': return 'ธ';
                case 'y': return 'ั';
                case 'Y': return 'ํ';
                case 'u': return 'ี';
                case 'U': return '๊';
                case 'i': return 'ร';
                case 'I': return 'ณ';
                case 'o': return 'น';
                case 'O': return 'ฯ';
                case 'p': return 'ย';
                case 'P': return 'ญ';
                case '[': return 'บ';
                case '{': return 'ฐ';
                case ']': return 'ล';
                case '}': return ',';


                case 'a': return 'ฟ';
                case 'A': return 'ฤ';
                case 's': return 'ห';
                case 'S': return 'ฆ';
                case 'd': return 'ก';
                case 'D': return 'ฏ';
                case 'f': return 'ด';
                case 'F': return 'โ';
                case 'g': return 'เ';
                case 'G': return 'ฌ';
                case 'h': return '้';
                case 'H': return '็';
                case 'j': return '่';
                case 'J': return '๋';
                case 'k': return 'า';
                case 'K': return 'ษ';
                case 'l': return 'ส';
                case 'L': return 'ศ';
                case ';': return 'ว';
                case ':': return 'ซ';
                case '\'': return 'ง';
                case '\"': return '.';

                case 'z': return 'ผ';
                case 'Z': return '(';
                case 'x': return 'ป';
                case 'X': return ')';
                case 'c': return 'แ';
                case 'C': return 'ฉ';
                case 'v': return 'อ';
                case 'V': return 'ฮ';
                case 'b': return 'ิ';
                case 'B': return 'ฺ';
                case 'n': return 'ื';
                case 'N': return '์';
                case 'm': return 'ท';
                case 'M': return '?';
                case ',': return 'ม';
                case '<': return 'ฒ';
                case '.': return 'ใ';
                case '>': return 'ฬ';
                case '/': return 'ฝ';
                case '?': return 'ฦ';
                default:
                    return c;
            }

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            doIt();
            copyButton.Enabled = !string.IsNullOrWhiteSpace(this.textBox1.Text);
            clearEncodeText.Enabled = !string.IsNullOrWhiteSpace(this.textBox1.Text);

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            doIt();
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            doIt();
        }

        private void doIt() {
            bool isEnToThai = radioButton1.Checked;
            bool isCapslock = checkCaplock.Checked;
            string temp = textBox1.Text.ToString();
            textBox2.Text = decodeTyping(temp, isEnToThai,isCapslock);
        }

        private void checkCaplock_CheckedChanged(object sender, EventArgs e)
        {
            doIt();
        }

        private void clearEncodeText_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
        }

       
    }
}